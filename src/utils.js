import urlResolve from "url-resolve"

import config from "../config.js"

export async function fetchCategory(fetch, { showId, slug }) {
  let category = null
  for (let page = 0; ; page++) {
    const url = urlResolve(config.apiUrl, `/c/${slug}.json?page=${page}`)
    let response = null
    try {
      response = await fetch(url)
    } catch (error) {
      throw `Error while fetching category at ${url}: ${error}`
    }
    let data = null
    try {
      data = await response.json()
    } catch (error) {
      throw `Error while converting category at ${url} to JSON: ${error}`
    }
    data.topic_list.topics = data.topic_list.topics.map(topicEmbed => {
      return {
        ...topicEmbed,
        showId,
      }
    })
    if (category === null) {
      category = data
    } else {
      category.topic_list.topics = [...category.topic_list.topics, ...data.topic_list.topics]
    }
    if (data.topic_list.topics.length < data.topic_list.per_page) {
      return category
    }
  }
}

export async function fetchTopic(fetch, id) {
  const url = urlResolve(config.apiUrl, `/t/${id}.json`)
  let response = null
  try {
    response = await fetch(url)
  } catch (error) {
    throw `Error while fetching topic at ${url}: ${error}`
  }
  try {
    return await response.json()
  } catch (error) {
    throw `Error while converting topic at ${url} to JSON: ${error}`
  }
}

export function toMinutesSeconds(seconds) {
  let sign = ""
  if (seconds < 0) {
    seconds = -seconds
    sign = "-"
  }
  let minutes = Math.floor(seconds / 60)
  let secondsString = `00${seconds - minutes * 60}`.slice(-2)
  return `${sign}${minutes}:${secondsString}`
}
