import compression from "compression"
import fs from "fs"
import http from "http"
import path from "path"
import Polka from "polka"
import sirv from "sirv"
import url from "url"
import WebSocket from "uws"

import * as sapper from "../__sapper__/server.js"
import "./global.scss"
import config from "../config.js"

const dataPath = path.resolve("data.json")
const { PORT, NODE_ENV } = process.env
const dev = NODE_ENV === "development"

function loadData(path) {
  const data = fs.existsSync(path) ? JSON.parse(fs.readFileSync(path)) : {}
  if (data.acceptedMessagesId) {
    // Remove duplicate IDs.
    data.acceptedMessagesId = data.acceptedMessagesId.reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [])
  } else {
    data.acceptedMessagesId = []
  }
  if (data.acceptedProjectsId) {
    // Remove duplicate IDs.
    data.acceptedProjectsId = data.acceptedProjectsId.reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [])
  } else {
    data.acceptedProjectsId = []
  }
  if (data.duration === undefined) data.duration = 60
  if (data.messagesCategory === undefined) data.messagesCategory = null
  if (data.nowPlaying === undefined) data.nowPlaying = "default"
  if (data.playingCategorySlug === undefined) data.playingCategorySlug = config.projectsCategory.slug
  if (data.playingMessageId === undefined) data.playingMessageId = null
  if (data.playingProjectId === undefined) data.playingProjectId = null
  if (data.playingTopicId === undefined) data.playingTopicId = null
  if (data.projectsCategory === undefined) data.projectsCategory = null
  if (data.rejectedMessagesId) {
    // Remove duplicate IDs.
    data.rejectedMessagesId = data.rejectedMessagesId.reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [])
  } else {
    data.rejectedMessagesId = []
  }
  if (data.rejectedProjectsId) {
    // Remove duplicate IDs.
    data.rejectedProjectsId = data.rejectedProjectsId.reduce((ids, id) => (ids.includes(id) ? ids : [...ids, id]), [])
  } else {
    data.rejectedProjectsId = []
  }
  data.seconds = null
  data.secondsRunning = false
  return data
}

function saveData(path, data) {
  fs.writeFileSync(path, JSON.stringify(data, null, 2))
}

function sendDataToWssClients(wss, data) {
  const dataString = JSON.stringify(data, null, 2)
  wss.clients.forEach(function each(client) {
    if (client.readyState === WebSocket.OPEN) {
      client.send(dataString)
    }
  })
}

const polka = Polka({
  server: http.createServer(),
}).use(
  compression({ threshold: 0 }),
  sirv("static", { dev }),
  sapper.middleware(),
)

const data = loadData(dataPath)
let seconds = 0

// https://zeit.co/blog/async-and-await
function sleep(time) {
  return new Promise(resolve => setTimeout(resolve, time))
}

async function ticTac() {
  while (true) {
    if (data.nowPlaying === "pitches") {
      if (data.playingTopicId !== null) {
        // if (seconds >= data.duration) {
        //   data.playingTopicId = null
        //   seconds = 0
        // }
        data.seconds = seconds
        sendDataToWssClients(adminWss, data)
        sendDataToWssClients(homeWss, data)
      }
      if (data.secondsRunning) {
        seconds += 1 // Must be just before sleep and end of loop.
      }
    } else if (data.nowPlaying === "slideshow") {
      if (seconds >= 10) {
        seconds = 0
        if (data.playingCategorySlug === config.projectsCategory.slug && data.acceptedMessagesId.length > 0) {
          data.playingCategorySlug = config.messagesCategory.slug
          let index = data.playingMessageId === null ? 0 : data.acceptedMessagesId.indexOf(data.playingMessageId)
          data.playingTopicId = data.playingMessageId =
            index < 0
              ? null
              : index + 1 < data.acceptedMessagesId.length
                ? data.acceptedMessagesId[index + 1]
                : data.acceptedMessagesId[0]
        } else {
          data.playingCategorySlug = config.projectsCategory.slug
          let index = data.playingProjectId === null ? 0 : data.acceptedProjectsId.indexOf(data.playingProjectId)
          data.playingTopicId = data.playingProjectId =
            index < 0
              ? null
              : index + 1 < data.acceptedProjectsId.length
                ? data.acceptedProjectsId[index + 1]
                : data.acceptedProjectsId[0]
        }
        // Don't save data to minimize the risk of concurrent savings.
        // saveData(dataPath, data)
        sendDataToWssClients(adminWss, data)
        sendDataToWssClients(homeWss, data)
      }
      seconds += 1 // Must be just before sleep and end of loop.
    } else {
      // data.nowPlaying === "default"
    }
    await sleep(1000)
  }
}

// const adminWss = new WebSocket.Server({ server: polka.server })
const adminWss = new WebSocket.Server({ noServer: true })
const homeWss = new WebSocket.Server({ noServer: true })

adminWss.on("connection", ws => {
  // console.log("adminWss Connection")
  ws.on("message", msg => {
    // console.log("adminWss received:", msg)
    const entry = JSON.parse(msg)
    if (entry.type === "acceptedMessagesId") {
      // const data = loadData(dataPath)
      data.acceptedMessagesId = entry.acceptedMessagesId
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "acceptedProjectsId") {
      // const data = loadData(dataPath)
      data.acceptedProjectsId = entry.acceptedProjectsId
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "duration") {
      data.duration = entry.duration
      // saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "messagesCategory") {
      // console.log(entry.messagesCategory)
      // const data = loadData(dataPath)
      data.messagesCategory = entry.messagesCategory
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "nowPlaying") {
      // const data = loadData(dataPath)
      data.nowPlaying = entry.nowPlaying
      seconds = data.seconds = 0
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "playingTopicId") {
      // const data = loadData(dataPath)
      data.playingTopicId = entry.playingTopicId
      seconds = data.seconds = 0
      data.secondsRunning = false
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "projectsCategory") {
      // console.log(entry.projectsCategory)
      // const data = loadData(dataPath)
      data.projectsCategory = entry.projectsCategory
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else if (entry.type === "rejectedMessagesId") {
      // const data = loadData(dataPath)
      data.rejectedMessagesId = entry.rejectedMessagesId
      saveData(dataPath, data)
    } else if (entry.type === "rejectedProjectsId") {
      // const data = loadData(dataPath)
      data.rejectedProjectsId = entry.rejectedProjectsId
      saveData(dataPath, data)
    } else if (entry.type === "secondsRunning") {
      // const data = loadData(dataPath)
      data.secondsRunning = entry.secondsRunning
      saveData(dataPath, data)
      sendDataToWssClients(homeWss, data)
    } else {
      console.error("Unknown message type:", entry.type)
    }
  })

  // const data = loadData(dataPath)
  ws.send(JSON.stringify(data, null, 2))
})

homeWss.on("connection", ws => {
  // console.log("homeWss Connection")
  ws.on("message", msg => {
    console.log("homeWss received:", msg)
  })

  const data = loadData(dataPath)
  ws.send(JSON.stringify(data, null, 2))
})

polka.server.on("upgrade", (request, socket, head) => {
  const pathname = url.parse(request.url).pathname
  // console.log("on upgrade", pathname)

  if (pathname === "/") {
    homeWss.handleUpgrade(request, socket, head, function done(ws) {
      homeWss.emit("connection", ws, request)
    })
  } else if (pathname === "/admin") {
    adminWss.handleUpgrade(request, socket, head, function done(ws) {
      adminWss.emit("connection", ws, request)
    })
  } else {
    socket.destroy()
  }
})

ticTac()
polka
  .listen(PORT, error => {
    if (error) {
      console.log(`Error when calling listen on port ${PORT}:`, error)
    }
  })
