export default {
  apiUrl: "https://forum.datafin.fr/",
  appUrl: "http://localhost:3000",
  appWsUrl: "ws://localhost:3000",
  banner: {
    backgroundImageUrl:
      "https://forum.datafin.fr/uploads/default/original/1X/4e04278e924a63144461036fe45ceb87c29a826a.png",
    buttonTitle: "Inscrivez-vous au hackathon",
    buttonUrl: "https://secure2.assemblee-nationale.fr//visites/listeParticipants/evenement/MZFAd4X6VU",
    logoUrl: "https://forum.datafin.fr/uploads/default/original/1X/0ccec392b57b81b41b235b327dd6d2132b292377.png",
    title: `\
<h3><small>Exploitez les données financières publiques !</small></h3>
<br>
<h4>Les 15 et 16 juin, à l'Assemblée nationale</h4>
`,
  },
  language: "fr",
  messagesCategory: {
    showId: false,
    slug: "messages-de-service",
  },
  projectsCategory: {
    showId: true,
    slug: "defis",
  },
  title: "#dataFin Ambiancer",
}
