# Ambiancer

_A web site to animate hackathons by displaying automatic slideshows using data retrieved with Discourse API_

Ambiancer can be used to:

* display a slide for each pitch, with a timer that rings when speaking time is exceeded
* display an automatic slideshow of service messages

The pitches, presentations and services messages must be written using [Discourse](https://www.discourse.org/) topics. Ambiancer just allows an administrator to manage them.

Ambiancer has been designed to work with [Vitrine](https://framagit.org/eraviart/vitrine), but it doesn't require it.

Usages examples:

* [#dataFin hackathon](https://datafin.fr/)

_Ambiancer_ is free and open source software.

* [software repository](https://framagit.org/eraviart/ambiancer)
* [GNU Affero General Public License version 3 or greater](https://framagit.org/eraviart/ambiancer/blob/master/LICENSE.md)
